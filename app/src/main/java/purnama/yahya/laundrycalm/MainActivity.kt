package purnama.yahya.laundrycalm

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin->{
                var email = edUserName.text.toString()
                var password = edPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()){
                    Toast.makeText(this,"Username / Password Tidak Boleh Kosong", Toast.LENGTH_LONG).show()
                }else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating...")
                    progressDialog.show()

                    progressDialog.hide()
                    if (email=="pemilik@laundry.com" && password=="calm123") {
                        Toast.makeText(this,"Sukses Login Pemilik", Toast.LENGTH_SHORT).show()
                    } else if (email=="kasir@laundry.com" && password=="calm123") {
                        val intent = Intent(this, KasirActivity::class.java)
                        intent.putExtra("username",edUserName.text.toString())
                        edUserName.setText("")
                        edPassword.setText("")
                        edUserName.requestFocus()
                        Toast.makeText(this,"Sukses Login Kasir", Toast.LENGTH_SHORT).show()
                        startActivity(intent)
                    } else {

                    }
                }
            }
        }
    }
}