package purnama.yahya.laundrycalm

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_scanqr.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ScanQRActivity: AppCompatActivity(),View.OnClickListener {

    lateinit var intentIntegrator : IntentIntegrator
    var daftarTrx = mutableListOf<HashMap<String, String>>()
    var url = "http://192.168.1.9/laundry-calm-web/show_data.php"
    var url2 = "http://192.168.1.9/laundry-calm-web/query_upd_del_ins.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanqr)

        intentIntegrator = IntentIntegrator(this)
        intentIntegrator.setOrientationLocked(true)

        var paket : Bundle? = intent.extras
        var teks = paket?.getString("username")
        Log.i("info","$teks")

        btnScanQR.setOnClickListener(this)
        btnCariTrx.setOnClickListener(this)
        btnConfirmTrx.setOnClickListener(this)
        btnKembaliPeminjaman.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()

        btnConfirmTrx.visibility = View.GONE
    }

//    fun showDataProduk(trxid: String) {
//        val request = object : StringRequest(
//            Request.Method.POST, url,
//            Response.Listener { response ->
//                daftarProduk.clear()
//                val jsonArray = JSONArray(response)
//                for (x in 0..(jsonArray.length() - 1)) {
//                    val jsonObject = jsonArray.getJSONObject(x)
//                    var produk = HashMap<String, String>()
//                    produk.put("TRXID", jsonObject.getString("TRXID"))
//                    produk.put("CUSTNAMA", jsonObject.getString("CUSTNAMA"))
//                    produk.put("TRSTATUS", jsonObject.getString("TRSTATUS"))
//                    produk.put("TRTOTAL", jsonObject.getString("TRTOTAL"))
//                    daftarProduk.add(produk)
//                }
//                trxAdapter.notifyDataSetChanged()
//            },
//            Response.ErrorListener { error ->
//                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT)
//                    .show()
//            }) {
//            override fun getParams(): MutableMap<String, String> {
//                val hm = HashMap<String, String>()
//                hm.put("TRXID", trxid)
//                return hm
//            }
//        }
//        val queue = Volley.newRequestQueue(this)
//        queue.add(request)
//    }

    fun showDataTrx(TRID: String) {
        val request = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener { response ->
                daftarTrx.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var trx = HashMap<String, String>()
                    trx.put("TRID", jsonObject.getString("TRID"))
                    trx.put("CUSTNAMA", jsonObject.getString("CUSTNAMA"))
                    trx.put("TRSTATUS", jsonObject.getString("TRSTATUS"))
                    trx.put("TRTOTAL", jsonObject.getString("TRTOTAL"))
                    daftarTrx.add(trx)
                }
                val data = daftarTrx.get(0)
                txKdTrans.setText(data.get("TRID"))
                txCustomer.setText(data.get("CUSTNAMA"))
                txStatusTrx.setText(data.get("TRSTATUS"))
                txTotalTrx.setText(data.get("TRTOTAL"))
                if (data.get("TRSTATUS") == "Diproses"){
                    txStatusTrx.setTextColor(Color.RED)
                    btnConfirmTrx.visibility = View.VISIBLE
                } else {
                    txStatusTrx.setTextColor(Color.GREEN)
                    btnConfirmTrx.visibility = View.GONE
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT)
                    .show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("TRID", TRID)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode: String) {
        val request = object : StringRequest(
            Method.POST, url2,
            Response.Listener { response ->
                Log.i("info", "[" + response + "]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataTrx(txKdTrans.text.toString())
                    txStatusTrx.setTextColor(Color.GREEN)
                    btnConfirmTrx.visibility = View.GONE
                } else {
                    Toast.makeText(this, "Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                when (mode) {
                    "konfirmasi" -> {
                        hm.put("mode", "konfirmasi")
                        hm.put("TRID", txKdTrans.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if (intentResult!=null){
            if(intentResult.contents!=null){
                edKdTrx.setText(intentResult.contents)
                showDataTrx(edKdTrx.text.toString().trim())
            }else{
                Toast.makeText(this,"Dibatalkan", Toast.LENGTH_SHORT).show()
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnScanQR ->{
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnCariTrx ->{
                showDataTrx(edKdTrx.text.toString().trim())
            }
            R.id.btnConfirmTrx -> {
                queryInsertUpdateDelete("konfirmasi")
            }
            R.id.btnKembaliPeminjaman ->{
                finish()
            }
        }
    }
}