package purnama.yahya.laundrycalm

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_kasir.*

class KasirActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var intentIntegrator : IntentIntegrator
    var username = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kasir)

        intentIntegrator = IntentIntegrator(this)

        var paket : Bundle? = intent.extras
        username = paket?.getString("username").toString()
        txUsernameUser.setText(username)

        btnLogout.setOnClickListener(this)
        btnPinjamSepeda.setOnClickListener(this)
        imageView5.setOnClickListener(this)
        imageView7.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPinjamSepeda->{
                var intent = Intent(this, ScanQRActivity::class.java)
                startActivity(intent)
            }
            R.id.imageView5->{
                var intent = Intent(this, ScanQRActivity::class.java)
                startActivity(intent)
            }
            R.id.imageView7->{
                var intent = Intent(this, ScanQRActivity::class.java)
                startActivity(intent)
            }
            R.id.btnLogout -> {
                finish()
            }
        }
    }
}